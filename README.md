# Présentation

Cette version légère du déployeur de TalanLabs fonctionne en ligne de commandes. Il fonctionne avec GitLab.

Il se présente sous la forme d'un .jar qui est lancé en background. Il est particulièrement adapté pour tourner avec la commande `screen` de Unix.

# Téléchargement

Pour télécharger le déployeur, cliquez sur [ce lien](https://gitlab.talanlabs.com/nicolas-poste/deployer-cmder/builds/8664/artifacts/download).

# Installation

Placer le .jar DeployerCmder-jar-with-dependencies.jar sur le file system de la machine où faire les déploiements.

Placer également un log4j.xml. Vous pouvez par exemple utiliser celui donné dans le fichier téléchargé.

Avec un utilisateur adapté, lancer le .jar : *`java -Dlog4j.info -Dlog4j.configuration=file:///chemin/du/log4j.xml -jar DeployerCmder-jar-with-dependencies.jar`*

Arguments : *[--(g)itlabUrl=<url>] [--gitlab(T)oken=<token>] [--(u)surp=<hostname>] [--proxy(L)ogin=<login>] [--proxy(P)assword=<password>] [projectName]*

# Fonctionnement du Déployeur

Le déployeur utilise l'API de GitLab pour scanner tous les nouveaux builds (rafraîchissement : toutes les minutes).

Si un build possède des artifacts, alors le déployeur va essayer de trouver le fichier .deploy.yml, où se trouvent les instructions pour la livraison. Voir plus bas pour un exemple de .deploy.yml

# Configuration du projet

Afin de déployer les artifacts dans GitLab, il faut utiliser la balise `artifacts` dans le fichier .gitlab-ci.yml

Cf http://docs.gitlab.com/ce/ci/build_artifacts/README.html

Exemples :

*`java -Dhttp.proxyHost=gelabpscddb01 -Dhttps.proxyHost=gelabpscddb01 -Dhttp.proxyPort=3128 -Dhttps.proxyPort=3128 -jar DeployerCmder-jar-with-dependencies.jar -u lissic-1.vinci-concessions.net vinci/lisea-app`*

*`-g=https://gitlab.talanlabs.com -T=123abc`*

Utiliser `-Dhttp.proxyHost`, `-Dhttps.proxyHost`, `-Dhttp.proxyPort`, `-Dhttps.proxyPort` pour setter le proxy, en complément avec `-proxyLogin` et `-proxyPassword`.
Attention, les -Dhttp* doivent précéder le -jar alors que les autres arguments, propres au déployeur, doivent le suivre.

Si toutefois vous ne souhaitez pas passer le login et le mot de passe en paramètre, utilisez la commande proxyAuth suivie du login, le mot de passe sera alors la prochaine instruction à taper.

# Enregistrement de la configuration

La commande `register` permet de sauvegarder la configuration (mot de passe du proxy compris) dans le fichier *deployer-config.properties*.
Lors du prochain lancement du déployeur, il suffira alors de taper la commande donnée dans la partie **Installation**


# Aide en ligne de commandes

Taper entrée, help, help *commande*

```
INFO  05-30 11:37:02 Supported commands: [help, spy, auto, start, stop, deploy, test, checkerStatus, kill, exit, usurp, proxyUrl, proxyPort, proxyAuth, register]  (DeployerEntryPoint.java:325)
help proxyAuth
INFO  05-30 11:37:11 > help proxyAuth  (DeployerEntryPoint.java:343)
INFO  05-30 11:37:11 PROXY_AUTH: *proxyAuth <proxyLogin>* - Sets the proxy login  (DeployerEntryPoint.java:257)
```

# Avec screen

Taper `screen -d -R deploy` pour récupérer ou créer la session "deploy".

Lancer le deployeur, utiliser des commandes ou laisser faire.

Se détacher de la session en faisant `ctrl + a, d`

# Tests "Smoke"

Des tests ["Smoke"](https://en.wikipedia.org/wiki/Smoke_testing_(software)) peuvent être lancés après un déploiement. Voir le projet [deployer-smoke](https://gitlab.talanlabs.com/nicolas-poste/deployer-smoke) pour savoir comment en avoir sur votre application.
Ces tests sont lancés via une servlet mise à disposition dans l'application. La servlet retourne un rapport pour la campagne de tests.

La commande `test <refName>` permet de lancer ces tests sans impacter le build GitLab en affichant simplement le rapport.

# .deploy.yml

Voici un exemple complet d'un fichier .deploy.yml

```
artifacts: # nom invariant pour les artifacts à récupérer de GitLab (zip)
  client: client/target/lisea-app-client.war
  integ: integ/target/lisea-app-integ.war

.default_commands: &default_commands # alias pour réutilisation simplifiée
  stop: # nom invariable pour les instruction stop, par instance
    webapps: # arrêt de l'instance webapps, les instructions ci-dessous peuvent utiliser les variables $FOLDER et $TIMESTAMP
      - /etc/init.d/tomcat8 stop
      - rm -f $FOLDER/lisea-app-client.war
      - rm -rf $FOLDER/lisea-app-client
      - rm -f $FOLDER/lisea-app-integ.war
      - rm -rf $FOLDER/lisea-app-integ
      - chmod 664 $FOLDER/../lisea.log
      - mv $FOLDER/../lisea.log $FOLDER/../lisea.log.$TIMESTAMP
  start: # nom invariable pour les instructions start, par instance
    webapps: # démarrage de l'instance webapps
      - /etc/init.d/tomcat8 start

environment: # nom invariant pour définir le bloc des environnements
  dev: # nom de l'environnement
    host: lissic-1.vinci-concessions.net # hôte (possible d'usurper l'hôte actuel avec --usurp)
    logfile: /srv/appli/logs/deploy.log # où sont stockés les logs (non utilisé pour le moment)
    instance: # instances tomcat/jboss
      webapps: # instance webapp
        folder: /srv/appli/webapps # home de cette instance, repris dans la variable $FOLDER
        artifact:
          - client # lien avec l'artifact
          - integ
    smokeEntrypoint: # la liste des points d'entrée pour les tests "Smoke"
      - integ::http://172.21.254.23:8080/lisea-app-integ/rest/smoke/run # ici, le nom du build externe qui apparaît sur GitLab sera "smoke-test_*integ*" et l'url est appelée par le déployeur après un déploiement

deploy_job: # on peut utiliser autant de jobs que nécessaire
  automatic: true # ce job est exécuté automatiquement
  environment: # ce job ne concerne que l'environnement de DEV, comme décrit dans la balise environment ci-dessus
    - dev # référence à l'environnement de DEV
  only:
    - master # ne concerne que la branche master
  <<: *default_commands # injection des branches "stop" et "start"
  comment: # sera rajouté au commentaire sur gitlab
    description: <172.21.254.23:8080/lisea-app-client/|Lien vers le SI commercial>
```
