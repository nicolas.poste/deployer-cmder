package com.synaptix.deployer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synaptix.gitlab.api.models.builds.GitLabBuild;

public class LatestChecker {

	private static final Log LOG = LogFactory.getLog(LatestChecker.class);

	private static final int MAX_THREAD_SIZE = 10;

	private final ProjectApi projectApi;

	private final IDeployer deployer;

	private final Map<String, Integer> lastDeployBuildPerRef;

	private final ExecutorService executorService;

	private final Map<UUID, DeployThread> threadMap;

	private boolean running;

	public LatestChecker(ProjectApi projectApi, IDeployer deployer) {
		super();

		this.projectApi = projectApi;
		this.deployer = deployer;
		this.lastDeployBuildPerRef = new HashMap<>();
		this.executorService = Executors.newFixedThreadPool(MAX_THREAD_SIZE);
		this.threadMap = new LinkedHashMap<>();

		checkLastBuilds(false);

		LOG.info("Latests founds: " + this.lastDeployBuildPerRef);

		running = true;
		launchChecker();
	}

	/**
	 * Force the deployment of a branch
	 */
	public void deploy(final String ref) {
		if (lastDeployBuildPerRef.containsKey(ref)) {
			lastDeployBuildPerRef.put(ref, -1);
		}
		checkLastBuilds(true);
	}

	private void checkLastBuilds(boolean verbose) {
		if (verbose) {
			LOG.info("Checking last builds...");
		}
		try {
			List<GitLabBuild> projectBuilds = projectApi.getProjectBuilds();
			Iterator<GitLabBuild> iterator = projectBuilds.stream().filter(b -> "success".equals(b.getStatus()) && b.getArtifactsFile() != null).iterator();
			if (verbose) {
				LOG.info("Last builds fetched, looping...");
			}
			while (iterator.hasNext()) {
				GitLabBuild build = iterator.next();
				Integer maxProject = lastDeployBuildPerRef.get(build.getRef());
				if (maxProject != null) {
					fromUpdate(build, maxProject, verbose);
				} else {
					fromInitOrNewBranch(verbose, build);
				}
			}
		} catch (IOException e) {
			LOG.error("", e);
		}
	}

	private void fromUpdate(GitLabBuild build, Integer maxProject, boolean verbose) {
		if (maxProject < build.getId()) {
			lastDeployBuildPerRef.put(build.getRef(), build.getId());
			if (verbose) {
				LOG.info("Deploying build: " + build.getId());
			}
			fireNewBuild(build, maxProject == -1 ? IDeployer.Mode.FORCE : IDeployer.Mode.AUTOMATIC);
		}
	}

	private void fromInitOrNewBranch(boolean verbose, GitLabBuild build) {
		lastDeployBuildPerRef.put(build.getRef(), build.getId());
		if (verbose) {
			if (running) {
				LOG.info("Deploying build: " + build.getId());
			} else {
				LOG.info("New build found but the checker is not running: " + build.getId());
			}
		}
		fireNewBuild(build, running ? IDeployer.Mode.AUTOMATIC : IDeployer.Mode.CHECK);
	}

	private void fireNewBuild(GitLabBuild build, IDeployer.Mode mode) {
		checkThreads();
		UUID uuid = UUID.randomUUID();
		DeployThread deployThread = new DeployThread(uuid, String.format("DEPLOY_%s_%s", build.getRef(), build.getId()));
		threadMap.put(uuid, deployThread);

		Runnable runnable = () -> {
			if (!threadMap.containsKey(uuid)) {
				return; // has been interrupted before beeing started
			}
			deployThread.setStatus("Running");
			Thread.currentThread().setName(deployThread.name);

			deployThread.setThread(Thread.currentThread());

			deployer.fireNewBuild(projectApi.getUsedProject(), build, mode);

			deployThread.setStatus("Done");

			threadMap.remove(uuid); // not running anymore
		};
		Future<?> future = executorService.submit(runnable);
		deployThread.setFuture(future);
	}

	private void displayThreadState() {
		checkThreads();
		LOG.info(String.format("Thread Pool size: %d over %d", threadMap.size(), MAX_THREAD_SIZE));
		threadMap.entrySet().forEach(entry -> LOG.info(entry.getValue()));
	}

	private void checkThreads() {
		Iterator<Map.Entry<UUID, DeployThread>> ite = threadMap.entrySet().iterator();
		while (ite.hasNext()) {
			Map.Entry<UUID, DeployThread> entry = ite.next();
			if (entry.getValue().isFinished()) {
				ite.remove();
			}

		}
		if (threadMap.size() >= MAX_THREAD_SIZE) {
			LOG.warn("The thread pool has reached its limit. Use *checkerStatus* to see the list and *kill <id>* to kill a thread.");
		}
	}

	public void killAll() {
		for (UUID u : threadMap.keySet()) {
			kill(u.toString());
		}
	}

	public void kill(String id) {
		UUID uuid = null;
		try {
			uuid = UUID.fromString(id);
		} catch (IllegalArgumentException e) {
			if (LOG.isTraceEnabled()) {
				LOG.trace("Not an UUID", e);
			}
			int count = 0;
			for (UUID u : threadMap.keySet()) {
				if (u.toString().startsWith(id)) {
					count++;
					uuid = u;
				}
			}
			if (count > 1) {
				LOG.info("There are more than one UUID matching the given ID");
				return;
			}
		}
		if (uuid == null) {
			LOG.info("No matching uuid found");
			return;
		}
		DeployThread deployThread = threadMap.get(uuid);
		if (deployThread == null) {
			LOG.info("There is no such thread");
		} else {
			threadMap.remove(uuid);
			deployThread.interrupt();
		}
	}

	private void launchChecker() {
		Thread th = new Thread(() -> {
			do {
				CountDownLatch cdl = new CountDownLatch(1);
				try {
					cdl.await(1, TimeUnit.MINUTES);
					if (running) {
						checkLastBuilds(false);
					}
				} catch (Exception e) {
					LOG.error("", e);
				}
			} while (running);
		});
		th.setName("CHECKER_THREAD");
		th.setDaemon(true);
		th.start();
	}

	boolean isRunning() {
		return running;
	}

	/**
	 * Stop or start the checker
	 */
	public void setRunning(boolean running) {
		if (!this.running && running) {
			launchChecker();
		}
		this.running = running;
	}

	/**
	 * Display the status of the checker. Is it running? What are the builds?
	 */
	public void displayStatus() {
		LOG.info("Checker status: " + (running ? "running" : "stopped"));
		LOG.info(lastDeployBuildPerRef);

		displayThreadState();
	}

	public void shutdown() {
		executorService.shutdown();
	}

	private class DeployThread {

		private final UUID uuid;

		private final String name;

		private String status;

		private Future<?> future;

		private Thread thread;

		private DeployThread(UUID uuid, String name) {
			this.uuid = uuid;
			this.name = name;
			this.status = "Pending";
		}

		public void setFuture(Future<?> future) {
			this.future = future;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		@Override
		public String toString() {
			return String.format("Thread %s (%s): status: %s, cancelled: %s, done: %s", uuid, name, status, future.isCancelled(), future.isDone());
		}

		public void setThread(Thread thread) {
			this.thread = thread;
		}

		public void interrupt() {
			if (thread != null) {
				thread.interrupt();
				this.setStatus("Interrupted");
			}
		}

		public boolean isFinished() {
			if ("Interrupted".equals(status)) {
				return true;
			}
			if (future != null && (future.isDone() || future.isCancelled())) {
				return true;
			}
			return false;
		}
	}
}
