package com.synaptix.deployer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.flywaydb.core.Flyway;
import org.yaml.snakeyaml.Yaml;

import com.synaptix.componentresteasy.SynaptixComponentModule;
import com.synaptix.deployer.exception.DeployException;
import com.synaptix.deployer.exception.DeployerConfigurationException;
import com.synaptix.deployer.model.CommitState;
import com.synaptix.deployer.model.Deploy;
import com.synaptix.deployer.model.IComment;
import com.synaptix.deployer.model.IEnvironment;
import com.synaptix.deployer.model.IInstance;
import com.synaptix.deployer.model.IJob;
import com.synaptix.deployer.smoke.SmokeTester;
import com.synaptix.deployer.smoke.model.SmokeTestReport;
import com.synaptix.gitlab.api.models.builds.GitLabBuild;
import com.synaptix.gitlab.api.models.projects.GitLabProject;

/**
 * Created by NicolasP on 07/04/2016.
 */
public class Deployer implements IDeployer {

	private static final Log LOG = LogFactory.getLog(Deployer.class);

	private static final String SMOKE_ENTRYPOINT_SEPARATOR = "::";

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");

	private final ProjectApi projectApi;

	private final ObjectMapper mapper;

	private final SmokeTester smokeTester;

	/**
	 * This map is used for start/stop commands: remembers last deploy file for each branch
	 */
	private final Map<String, Deploy> deployBranchMap;

	private String hostName;

	public Deployer(ProjectApi api, String usurpHostName) {
		super();

		this.projectApi = api;

		this.mapper = new ObjectMapper();
		this.mapper.registerModule(new SynaptixComponentModule());
		this.mapper.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);

		this.smokeTester = new SmokeTester();

		this.deployBranchMap = new HashMap<>();

		setHostName(usurpHostName);
	}

	private String getDefaultHostName() {
		try {
			return executeCommand("hostname", null);
		} catch (DeployException e) {
			LOG.fatal("Couldn't determine the hostname", e);
			throw new DeployerConfigurationException(e);
		}
	}

	public String getHostName() {
		return hostName;
	}

	@Override
	public void setHostName(String hostName) {
		this.hostName = hostName != null ? hostName : getDefaultHostName();
		LOG.info("Hostname is " + this.hostName);
	}

	@Override
	public void fireNewBuild(GitLabProject project, GitLabBuild build, Mode mode) {
		byte[] deployFile = projectApi.getFileContent(build.getCommit().getId(), ".deploy.yml");
		if (deployFile == null || deployFile.length == 0) {
			return;
		}

		String deployDescription = new String(deployFile);
		if (LOG.isDebugEnabled()) {
			LOG.debug(deployDescription);
		}

		LOG.info("Read .deploy.yml successful of build " + build.getId());
		Deploy deploy = parse(deployDescription);
		deploy.setBuild(build);
		deployBranchMap.put(build.getRef(), deploy);

		IEnvironment environment = getEnvironment(deploy);
		if (environment == null) {
			LOG.info("This deploy is not concerned for " + hostName);
			return;
		}

		if (mode == Mode.AUTOMATIC || mode == Mode.FORCE) {
			List<IJob> jobList = getJobs(deploy, environment, build.getRef(), mode);
			if (!jobList.isEmpty()) {
				LOG.info(String.format("Found %d jobs", jobList.size()));
				runJobs(project, build, deploy, environment, jobList);
			} else {
				LOG.info(String.format("No job found for %s (mode: %s)", environment.getName(), mode));
			}
		}
	}

	private void runJobs(GitLabProject project, GitLabBuild build, Deploy deploy, IEnvironment environment, List<IJob> jobList) {
		File file = null;
		try {
			file = File.createTempFile("test", ".zip");
			LOG.info("Downloading zip of build " + build.getId());
			projectApi.downloadUrl(build, file.toURI());
			LOG.info("Reading zip of build " + build.getId());
			byte[] bytes = Files.readAllBytes(Paths.get(file.toURI()));
			LOG.info("File read successfully");
			deployOnEnvironment(project, build, bytes, deploy, environment, jobList);

			test(environment, build, false);

		} catch (IOException e) {
			LOG.error("Run jobs error", e);
		} finally {
			LOG.info("Fire finished");
			if (file != null) {
				LOG.info("Deleting file " + file.getName());
				boolean delete;
				try {
					delete = file.delete();
				} catch (Exception e) {
					LOG.error("", e);
					delete = false;
				}
				if (delete) {
					LOG.info("Delete file successful");
				} else {
					LOG.warn("Delete file failed!");
					file.deleteOnExit();
				}
			}
		}
	}

	@Override
	public void test(String ref) {
		Deploy deploy = deployBranchMap.get(ref);
		if (deploy == null) {
			LOG.warn("This branch hasn't been scanned. Cannot proceed to start the application.");
			return;
		}
		IEnvironment environment = getEnvironment(deploy);
		if (environment != null) {
			test(environment, deploy.getBuild(), true);
		}
	}

	/**
	 * Test all entry points
	 *
	 * @param environment reflects the concerned environment, on which we are testing
	 * @param build       is the last valid build for the wanted branch
	 * @param force       indicates if the user is at the source or if is part of a deployment
	 */
	private void test(IEnvironment environment, GitLabBuild build, boolean force) {
		if (environment.getSmokeEntrypoint() != null && !environment.getSmokeEntrypoint().isEmpty()) {
			String commitHash = build.getCommit().getId();
			if (!force) {
				for (String smokeEntrypoint : environment.getSmokeEntrypoint()) {
					projectApi.statusCommit(commitHash, CommitState.PENDING, build.getRef(), getSmokeAlias(smokeEntrypoint), null, "Waiting to test...");
				}
			}
			for (String smokeEntrypoint : environment.getSmokeEntrypoint()) {
				testSmokeEntrypoint(smokeEntrypoint, build, commitHash, force);
			}
		}
	}

	private void testSmokeEntrypoint(String smokeEntrypoint, GitLabBuild build, String commitHash, boolean force) {
		String smokeAlias = getSmokeAlias(smokeEntrypoint);
		String smokeUrl = getSmokeUrl(smokeEntrypoint);
		if (!force) {
			projectApi.statusCommit(commitHash, CommitState.RUNNING, build.getRef(), smokeAlias, null, "Testing...");
		}
		try {
			SmokeTestReport smokeTestReport = smokeTester.testSmokeEntryPoint(smokeUrl);
			if (smokeTestReport != null && smokeTestReport.getData() != null) {
				smokeTestReport = smokeTestReport.getData(); // fix if data has the report
			}
			if (force) {
				if (smokeTestReport.isTestsValid()) {
					LOG.info(String.format("Smoke test succeeded for entrypoint: %s%s%s", smokeEntrypoint, System.lineSeparator(), smokeTestReport));
				} else {
					LOG.error(String.format("Smoke test failed for entrypoint (forced): %s%s%s", smokeEntrypoint, System.lineSeparator(), smokeTestReport));
				}
			} else {
				if (smokeTestReport.isTestsValid()) {
					projectApi.statusCommit(commitHash, CommitState.SUCCESS, build.getRef(), smokeAlias, null, "Success");
				} else {
					projectApi.statusCommit(commitHash, CommitState.FAILED, build.getRef(), smokeAlias, null, smokeTestReport.toString());
					String errorMessage = String.format("Smoke test failed for entrypoint: %s%s%s", smokeEntrypoint, System.lineSeparator(), smokeTestReport);
					projectApi.commentCommit(commitHash, errorMessage);
					LOG.error(errorMessage);
				}
			}
		} catch (Exception e) {
			if (!force) {
				projectApi.statusCommit(commitHash, CommitState.FAILED, build.getRef(), smokeAlias, null, e.getMessage());
				projectApi.commentCommit(commitHash, "Smoke test exception for entrypoint: " + smokeEntrypoint);
			}
			LOG.error("Smoke test exception for entrypoint: " + smokeEntrypoint, e);
		}
	}

	private String getSmokeAlias(String smokeEntrypoint) {
		String[] split = smokeEntrypoint.split(SMOKE_ENTRYPOINT_SEPARATOR, 2);
		return "smoke-test_" + split[0];
	}

	private String getSmokeUrl(String smokeEntrypoint) {
		String[] split = smokeEntrypoint.split(SMOKE_ENTRYPOINT_SEPARATOR, 2);
		if (split.length > 1) {
			return split[1];
		}
		return smokeEntrypoint;
	}

	@Override
	public void start(GitLabProject project, String ref) {
		Deploy deploy = deployBranchMap.get(ref);
		if (deploy == null) {
			LOG.warn("This branch hasn't been scanned. Cannot proceed to start the application.");
			return;
		}
		GitLabBuild build = deploy.getBuild();
		IEnvironment environment = getEnvironment(deploy);
		List<IJob> jobs = getJobs(deploy, environment, ref, Mode.FORCE);
		if (jobs.isEmpty()) {
			return;
		}
		for (IJob job : jobs) {
			Map<IInstance, Map<String, String>> varMap = new HashMap<>();
			for (Map.Entry<String, IInstance> entry : environment.getInstance().entrySet()) {
				varMap.put(entry.getValue(), buildVarSubstitionMap(environment, entry.getValue()));
			}

			try {
				String successMessage = buildSuccessMessage(project, job, build, environment);
				start(job, build, environment, varMap, successMessage);
			} catch (DeployException e) {
				LOG.error(String.format("Start of job %s failed", job.getName()), e);
			}
		}
	}

	@Override
	public void stop(GitLabProject project, String ref) {
		Deploy deploy = deployBranchMap.get(ref);
		if (deploy == null) {
			LOG.warn("This branch hasn't been scanned. Cannot proceed to stop the application.");
			return;
		}
		IEnvironment environment = getEnvironment(deploy);
		List<IJob> jobs = getJobs(deploy, environment, ref, Mode.FORCE);
		if (jobs.isEmpty()) {
			return;
		}
		for (IJob job : jobs) {
			Map<IInstance, Map<String, String>> varMap = new HashMap<>();
			for (Map.Entry<String, IInstance> entry : environment.getInstance().entrySet()) {
				varMap.put(entry.getValue(), buildVarSubstitionMap(environment, entry.getValue()));
			}

			try {
				stop(job, environment, varMap);
			} catch (DeployException e) {
				LOG.error(String.format("Stop of job %s failed", job.getName()), e);
			}
		}
	}

	private void deployOnEnvironment(GitLabProject project, GitLabBuild build, byte[] bytes, Deploy deploy, IEnvironment environment, List<IJob> jobList) {
		// update commit status
		for (IJob job : jobList) {
			projectApi.statusCommit(build.getCommit().getId(), CommitState.PENDING, build.getRef(), job.getName(), environment.getName(), "Waiting to deploy...");
		}
		boolean stop = false;
		for (IJob job : jobList) {
			stop = iterateJob(project, build, bytes, deploy, environment, stop, job);
		}
	}

	private boolean iterateJob(GitLabProject project, GitLabBuild build, byte[] bytes, Deploy deploy, IEnvironment environment, final boolean stop, IJob job) {
		boolean shouldStop = stop;
		try {
			if (shouldStop) {
				projectApi.statusCommit(build.getCommit().getId(), CommitState.CANCELED, build.getRef(), job.getName(), environment.getName(), "Canceled");
			} else {
				deployJob(project, build, bytes, deploy, environment, job);
			}
		} catch (Exception e) {
			LOG.error("Job exception", e);
			shouldStop = true;
		}
		return shouldStop;
	}

	private List<IJob> getJobs(Deploy deploy, IEnvironment environment, String ref, Mode mode) {
		Stream<IJob> stream = deploy.getJobs().stream();
		if (mode == Mode.AUTOMATIC) {
			stream = stream.filter(IJob::isAutomatic);
		}
		if (mode != Mode.FORCE) {
			stream = stream.filter(job -> job.getOnly() != null && job.getOnly().contains(ref));
		}
		return stream.filter(job -> job.getEnvironment() != null && job.getEnvironment().contains(environment.getName())).collect(Collectors.toList());
	}

	private void deployJob(GitLabProject project, GitLabBuild build, byte[] bytes, Deploy deploy, IEnvironment environment, IJob job) {
		try {
			projectApi.statusCommit(build.getCommit().getId(), CommitState.RUNNING, build.getRef(), job.getName(), environment.getName(), "Deploying...");
			deploy(deploy, job, environment, project, build, bytes);
			projectApi.statusCommit(build.getCommit().getId(), CommitState.SUCCESS, build.getRef(), job.getName(), environment.getName(), "Deployed");
		} catch (Exception e) {
			projectApi.statusCommit(build.getCommit().getId(), CommitState.FAILED, build.getRef(), job.getName(), environment.getName(), "Failed" + (job.getCurrentStep() != null ? " during " + job.getCurrentStep() : null));
			notifyFailure(project, build, environment, job, e);
		}
	}

	private void notifyFailure(GitLabProject project, GitLabBuild build, IEnvironment environment, IJob job, Exception e) {
		String message = String.format("<%s|%s>: [FAILURE] Job %s for build <%s/commit/%s|%s> of <%s/commits/%s|%s> branch has failed on the %s environment", project.getWebUrl(), project.getPathWithNamespace(), job.getName(), project.getWebUrl(), build.getCommit().getId(), build.getCommit().getShortId(), project.getWebUrl(), build.getRef(), build.getRef(), environment.getName());
		StringBuilder sb = new StringBuilder("```\n");
		printStack(e, sb);
		sb.append("\n```");
		notifyDeploy(build.getCommit().getId(), message, job.getComment(), sb.toString());
		LOG.error(message, e);
	}

	private void printStack(Throwable e, StringBuilder sb) {
		sb.append(e);
		for (StackTraceElement element : e.getStackTrace()) {
			sb.append("\n\tat ").append(element);
		}
		if (e.getCause() != null) {
			sb.append("\nCaused by: ");
			printStack(e.getCause(), sb);
		}
	}

	private IEnvironment getEnvironment(Deploy deploy) {
		for (Map.Entry<String, IEnvironment> entry : deploy.getEnvironment().entrySet()) {
			IEnvironment environment = entry.getValue();
			if (hostName.equals(environment.getHost())) {
				environment.setName(entry.getKey());
				return environment;
			}
		}
		LOG.info("No definition found for current environment");
		return null;
	}

	private void notifyDeploy(String commitHash, final String message, IComment comment, String description) {
		String m = message;
		if (description != null) {
			m = m + "\n" + description;
		}
		if (comment != null) {
			m = m + "\n" + comment.getDescription();
		}
		projectApi.commentCommit(commitHash, m);
		LOG.info(message);
	}

	private void deploy(Deploy deploy, IJob job, IEnvironment environment, GitLabProject project, GitLabBuild build, byte[] bytes) throws DeployException {
		Map<IInstance, Map<String, String>> varMap = new HashMap<>();
		Map<String, String> generalVarSubstitutionMap = buildGeneralVarSubstitutionMap(environment);
		for (Map.Entry<String, IInstance> entry : environment.getInstance().entrySet()) {
			varMap.put(entry.getValue(), buildVarSubstitionMap(environment, entry.getValue()));
		}

		stop(job, environment, varMap);

		flyway(job);

		clean(job, environment, varMap);

		for (IInstance instance : environment.getInstance().values()) {
			String folder = instance.getFolder();
			List<String> artifacts = instance.getArtifact();
			for (String artifact : artifacts) {
				String filePath = deploy.getArtifacts().get(artifact);
				copyFile(bytes, folder, filePath, generalVarSubstitutionMap);
			}
		}

		String successMessage = buildSuccessMessage(project, job, build, environment);
		start(job, build, environment, varMap, successMessage);
	}

	private String buildSuccessMessage(GitLabProject project, IJob job, GitLabBuild build, IEnvironment environment) {
		return String.format("<%s|%s>: Job %s for build <%s/commit/%s|%s> of <%s/commits/%s|%s> branch has been completed on the %s environment", project.getWebUrl(), project.getPathWithNamespace(), job.getName(), project.getWebUrl(), build.getCommit().getId(), build.getCommit().getShortId(), project.getWebUrl(), build.getRef(), build.getRef(), environment.getName());
	}

	private void stop(IJob job, IEnvironment environment, Map<IInstance, Map<String, String>> varMap) throws DeployException {
		if (job.getStop() == null) {
			return;
		}
		job.setCurrentStep("stop");
		LOG.info("Stopping instances...");
		for (Map.Entry<String, IInstance> entry : environment.getInstance().entrySet()) {
			executeCommands(job.getStop().get(entry.getKey()), varMap.get(entry.getValue()));
		}
		LOG.info("Instances stopped");
	}

	private void flyway(IJob job) throws DeployException {
		if (job.getFlyway() == null || StringUtils.isBlank(job.getFlyway().getConfigFile())) {
			return;
		}
		job.setCurrentStep("flyway");
		LOG.info("Starting flyway...");
		Flyway flyway = new Flyway();

		Properties properties = new Properties();
		try (FileReader reader = new FileReader(job.getFlyway().getConfigFile())) {
			properties.load(reader);
		} catch (Exception e) {
			throw new DeployException(e);
		}

		flyway.configure(properties);

		flyway.setBaselineOnMigrate(true);
		flyway.setOutOfOrder(true);
		int nb = flyway.migrate();
		LOG.info("Flyway ran: " + nb);
	}

	private void clean(IJob job, IEnvironment environment, Map<IInstance, Map<String, String>> varMap) throws DeployException {
		if (job.getClean() == null) {
			return;
		}
		job.setCurrentStep("clean");
		LOG.info("Cleaning instances...");
		for (Map.Entry<String, IInstance> entry : environment.getInstance().entrySet()) {
			executeCommands(job.getClean().get(entry.getKey()), varMap.get(entry.getValue()));
		}
		LOG.info("Instances stopped");
	}

	private void start(IJob job, GitLabBuild build, IEnvironment environment, Map<IInstance, Map<String, String>> varMap, String successMessage) throws DeployException {
		if (job.getStart() == null) {
			return;
		}
		job.setCurrentStep("start");
		LOG.info("Starting instances...");
		for (Map.Entry<String, IInstance> entry : environment.getInstance().entrySet()) {
			executeCommands(job.getStart().get(entry.getKey()), varMap.get(entry.getValue()));
			notifyDeploy(build.getCommit().getId(), successMessage, job.getComment(), null);
		}
		LOG.info("Instances started...");
	}

	private void copyFile(byte[] bytes, String folder, final String originalFileFilter, Map<String, String> varMap) throws DeployException {
		try {
			LOG.info("Getting zipped content...");
			String fileFilter = evaluateString(originalFileFilter, varMap);
			Map<String, byte[]> fileMap = getZippedContent(bytes, fileFilter);
			if (!fileMap.isEmpty()) {
				for (Map.Entry<String, byte[]> entry : fileMap.entrySet()) {
					String fileName = entry.getKey();
					byte[] fileBytes = entry.getValue();
					File destination = new File(folder, fileName.substring(fileName.lastIndexOf(File.separator) + 1));
					dumpFile(fileBytes, destination, fileName);
				}
			} else {
				LOG.warn(String.format("The file %s has not been found.", fileFilter));
			}
		} catch (IOException e) {
			throw new DeployException("Copy file exception", e);
		}
	}

	private void dumpFile(byte[] fileBytes, File destination, String fileName) throws DeployException {
		try {
			FileUtils.writeByteArrayToFile(destination, fileBytes);
		} catch (IOException e) {
			throw new DeployException(String.format("Error copying %s", fileName), e);
		}
	}

	private void executeCommands(List<String> cmds, Map<String, String> varSubstitionMap) throws DeployException {
		if (cmds != null) {
			for (String cmd : cmds) {
				String result = executeCommand(cmd, varSubstitionMap);
				if (LOG.isDebugEnabled()) {
					LOG.debug(result);
				}
			}
		}
	}

	private Map<String, String> buildVarSubstitionMap(IEnvironment environment, IInstance instance) {
		Map<String, String> varMap = buildGeneralVarSubstitutionMap(environment);
		varMap.put("FOLDER", instance.getFolder());
		return varMap;
	}

	private Map<String, String> buildGeneralVarSubstitutionMap(IEnvironment environment) {
		Map<String, String> varMap = new HashMap<>();
		varMap.put("TIMESTAMP", sdf.format(new Date()));
		varMap.put("ENVIRONMENT", environment.getName());
		return varMap;
	}

	private Deploy parse(String deployDescription) {
		Yaml yaml = new Yaml();
		Map<String, Object> map = (Map<String, Object>) yaml.load(deployDescription);

		return mapper.convertValue(map, Deploy.class);
	}

	public Map<String, byte[]> getZippedContent(byte[] bytes, final String originalFileFilter) throws IOException {
		Map<String, byte[]> fileMap = new HashMap<>();
		String fileFilter = originalFileFilter;
		if (!fileFilter.startsWith("/") && !fileFilter.endsWith("/")) { // is not a /[regex]/
			fileFilter = "^" + fileFilter.replaceAll("\\.", "\\\\.").replaceAll("\\*", "[^\\/]*");
		}

		Pattern patt = Pattern.compile(fileFilter, Pattern.CASE_INSENSITIVE);

		try (ByteArrayInputStream bais = new ByteArrayInputStream(bytes); ZipInputStream zis = new ZipInputStream(bais)) {
			ZipEntry entry;
			while ((entry = zis.getNextEntry()) != null) {
				String fileName = entry.getName();
				if (!entry.isDirectory() && patt.matcher(fileName).matches()) {
					byte[] result = IOUtils.toByteArray(zis);
					fileMap.put(fileName, result);
				}
			}
		}
		return fileMap;
	}

	private String executeCommand(String cmd, Map<String, String> varSubstitionMap) throws DeployException {
		String line;
		try {

			String execCmd = evaluateString(cmd, varSubstitionMap);
			if (LOG.isDebugEnabled()) {
				LOG.debug("Executing cmd " + cmd);
			}
			Process process = Runtime.getRuntime().exec(execCmd);

			BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
			StringBuilder sb = new StringBuilder();
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			line = sb.toString();
			boolean done = process.waitFor(5, TimeUnit.MINUTES);
			if (!done) {
				throw new DeployException(String.format("Timeout exception. Command: %s", execCmd));
			}
		} catch (IOException | InterruptedException e) {
			throw new DeployException("Execute command exception", e);
		}
		return line;
	}

	public String evaluateString(final String cmd, Map<String, String> varSubstitionMap) {
		String c = cmd;
		Pattern patt = Pattern.compile("\\$([a-z_]+)", Pattern.CASE_INSENSITIVE);
		boolean replaced;
		do {
			replaced = false;
			Matcher matt = patt.matcher(c);
			while (matt.find()) {
				String g = matt.group(1);
				String replacement = varSubstitionMap.get(g.toUpperCase());
				if (replacement != null) {
					Pattern p = Pattern.compile(String.format("\\$(%s+)", g), Pattern.CASE_INSENSITIVE);
					Matcher m = p.matcher(c);
					c = m.replaceAll(replacement);
					replaced = true;
				}
			}
		} while (replaced);
		return c;
	}
}
