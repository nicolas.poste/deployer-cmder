package com.synaptix.deployer.exception;

/**
 * Created by NicolasP on 13/06/2016.
 */
public class SmokeTestException extends Exception {

	public SmokeTestException(String message) {
		super(message);
	}

	public SmokeTestException(String message, Throwable cause) {
		super(message, cause);
	}
}
