package com.synaptix.deployer.exception;

/**
 * Created by NicolasP on 14/04/2016.
 */
public class DeployException extends Exception {

	public DeployException(Throwable cause) {
		super(cause);
	}

	public DeployException(String message) {
		super(message);
	}

	public DeployException(String message, Throwable cause) {
		super(message, cause);
	}
}
