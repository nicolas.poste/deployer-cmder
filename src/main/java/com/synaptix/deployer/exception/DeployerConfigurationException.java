package com.synaptix.deployer.exception;

/**
 * Created by NicolasP on 18/04/2016.
 */
public class DeployerConfigurationException extends RuntimeException {

	public DeployerConfigurationException(String message) {
		super(message);
	}

	public DeployerConfigurationException(Throwable e) {
		super(e);
	}
}
