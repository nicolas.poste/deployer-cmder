package com.synaptix.deployer.smoke;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.synaptix.deployer.smoke.exception.SmokeDeserializationException;
import com.synaptix.deployer.smoke.model.SmokeTestReport;

/**
 * Created by NicolasP on 13/06/2016.
 */
public class SmokeReportDeserializer {

	private final ObjectMapper mapper;

	public SmokeReportDeserializer() {
		mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}

	public final ObjectMapper getMapper() {
		return mapper;
	}

	public SmokeTestReport deserialize(String report) throws SmokeDeserializationException {
		try {
			return mapper.readValue(report, SmokeTestReport.class);
		} catch (IOException e) {
			throw new SmokeDeserializationException("Couldn't deserialize smoke report", e);
		}
	}
}
