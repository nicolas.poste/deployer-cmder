package com.synaptix.deployer.smoke;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.synaptix.deployer.exception.SmokeTestException;
import com.synaptix.deployer.smoke.model.SmokeTestReport;

/**
 * Created by NicolasP on 13/06/2016.
 */
public class SmokeTester {

	private static final String LINE_SEPARATOR = System.lineSeparator();

	private final SmokeReportDeserializer deserializer;

	public SmokeTester() {
		this.deserializer = new SmokeReportDeserializer();
	}

	public SmokeTestReport testSmokeEntryPoint(String url) throws SmokeTestException {
		HttpClient httpclient = HttpClientBuilder.create().build();
		HttpGet httpGet = new HttpGet(url);
		try {
			httpGet.setHeader("Accept", "application/json");
			httpGet.setHeader("Content-Type", "application/json");

			HttpResponse httpResponse = httpclient.execute(httpGet);

			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				throw new SmokeTestException("Error testing smoke entry point: HTTP error code : " + httpResponse.getStatusLine().getStatusCode());
			}

			String report = readResponse(httpResponse.getEntity());
			return deserializer.deserialize(report);

		} catch (SmokeTestException e) {
			throw e;
		} catch (Exception e) {
			throw new SmokeTestException("Error reading response", e);
		} finally {
			httpGet.releaseConnection();
		}
	}

	private String readResponse(HttpEntity httpEntity) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(httpEntity.getContent()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = br.readLine()) != null) {
			if (sb.length() > 0) {
				sb.append(LINE_SEPARATOR);
			}
			sb.append(line);
		}
		return sb.toString();
	}
}
