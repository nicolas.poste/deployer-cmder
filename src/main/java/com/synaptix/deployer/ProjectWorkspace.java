package com.synaptix.deployer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synaptix.deployer.exception.DeployerConfigurationException;
import com.synaptix.deployer.model.ConfigKey;
import com.synaptix.deployer.model.DeployerProperties;
import com.synaptix.gitlab.api.models.projects.GitLabProject;

/**
 * Created by NicolasP on 27/06/2016.
 */
class ProjectWorkspace {

	private static final Log LOG = LogFactory.getLog(ProjectWorkspace.class);

	private final ProjectApi projectApi;

	private final Deployer deployer;

	private final LatestChecker checker;

	private final DeployerProperties deployerProperties;

	ProjectWorkspace(String projectName, DeployerProperties myDeployerProperties) {
		this.deployerProperties = new DeployerProperties();
		this.deployerProperties.putAll(myDeployerProperties);

		this.projectApi = new ProjectApi(deployerProperties.getProperty(ConfigKey.GITLAB_URL), deployerProperties.getProperty(ConfigKey.GITLAB_TOKEN));
		deployer = new Deployer(projectApi, deployerProperties.getProperty(ConfigKey.HOST_NAME));
		GitLabProject usedProject = projectApi.useProject(projectName);

		if (usedProject != null) {
			checker = new LatestChecker(projectApi, deployer);
		} else {
			throw new DeployerConfigurationException("GitLab could not be contacted or project is unknown");
		}
	}

	DeployerProperties getParameters() {
		DeployerProperties myDeployerProperties = new DeployerProperties();
		myDeployerProperties.putAll(this.deployerProperties);
		return myDeployerProperties;
	}

	String getProjectName() {
		return projectApi.getUsedProject().getPathWithNamespace();
	}

	/**
	 * Stop or start the checker
	 */
	void setRunning(boolean running) {
		checker.setRunning(running);
	}

	private String getBranch(String refName) {
		String ref = refName;
		if (StringUtils.isEmpty(ref)) {
			ref = "master"; //$NON-NLS-1$
		}
		return ref;
	}

	/**
	 * Force the deployment of a branch (default: master)
	 */
	public void deploy(final String refName) {
		String ref = getBranch(refName);
		checker.deploy(ref);
	}

	void start(String refName) {
		deployer.start(projectApi.getUsedProject(), getBranch(refName));
	}

	void stop(String refName) {
		if (checker.isRunning()) {
			LOG.info("The checker is still running. You might want to stop it too...");
		}
		deployer.stop(projectApi.getUsedProject(), getBranch(refName));
	}

	void test(final String refName) {
		deployer.test(getBranch(refName));
	}

	/**
	 * Display the status of the checker. Is it running? What are the builds?
	 */
	void displayStatus() {
		checker.displayStatus();
	}

	void kill(String id) {
		checker.kill(id);
	}

	void setHostName(String hostName) {
		deployer.setHostName(hostName);
		deployerProperties.setProperty(ConfigKey.HOST_NAME, hostName);
	}

	void destroy() {
		checker.setRunning(false);
		checker.killAll();
	}
}
