package com.synaptix.deployer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synaptix.deployer.model.CommitState;
import com.synaptix.gitlab.api.GitLabAPI;
import com.synaptix.gitlab.api.Paged;
import com.synaptix.gitlab.api.Pagination;
import com.synaptix.gitlab.api.models.builds.GitLabBuild;
import com.synaptix.gitlab.api.models.projects.GitLabProject;

/**
 * An API around a project in particular
 */
public final class ProjectApi {

	private static final Log LOG = LogFactory.getLog(ProjectApi.class);

	private final GitLabAPI api;

	private final String gitLabUrl;

	private final String gitLabToken;

	private GitLabProject gitLabProject;

	public ProjectApi(String gitLabUrl, String gitLabToken) {
		super();

		api = GitLabAPI.connect(gitLabUrl, gitLabToken);
		this.gitLabUrl = gitLabUrl;
		this.gitLabToken = gitLabToken;
	}

	public GitLabProject useProject(String projectName) {
		try {
			gitLabProject = api.getGitLabAPIProjects().getProject(projectName);
		} catch (IOException e) {
			LOG.error("", e);
		}
		return gitLabProject;
	}

	public String getGitLabUrl() {
		return gitLabUrl;
	}

	public String getGitLabToken() {
		return gitLabToken;
	}

	public GitLabProject getUsedProject() {
		return gitLabProject;
	}

	public List<GitLabBuild> getProjectBuilds() throws IOException {
		Paged<GitLabBuild> gitLabBuildPaged = api.getGitLabAPIBuilds().getAllBuilds(gitLabProject.getId(), Pagination.create(0, 40), "success");
		return gitLabBuildPaged.getResults();
	}

	/**
	 * Download artifacts
	 */
	public void downloadUrl(GitLabBuild build, URI destination) {
		LOG.debug("Trying to download artifacts...");

		try {
			byte[] bytes = api.getGitLabAPIBuilds().getBuildArtifacts(gitLabProject.getId(), build.getId());
			Files.copy(new ByteArrayInputStream(bytes), Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			LOG.error("", e);
		}
	}

	/**
	 * Send a commit message on gitlab
	 */
	public void commentCommit(String commitHash, String message) {
		if (commitHash == null) {
			// webhook is mandatory
			return;
		}

		try {
			api.getGitLabAPICommits().postCommitComments(gitLabProject.getId(), commitHash, message, null, null, null);
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	/**
	 * Send a status message on gitlab
	 */
	public void statusCommit(String commitHash, CommitState state, String ref, String job, String environment, String message) {
		if (commitHash == null) {
			// webhook is mandatory
			return;
		}

		try {
			api.getGitLabAPICommits().postCommitStatus(gitLabProject.getId(), commitHash, state.toString(), ref, job + (environment != null ? "_" + environment : ""), null, message);
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	public byte[] getFileContent(String commitHash, String filePath) {
		try {
			return api.getGitLabAPIRepositories().getRawFileContent(gitLabProject.getId(), commitHash, filePath);
		} catch (Exception e) {
			LOG.info("Couldn't get .deploy.yml");
			if (LOG.isTraceEnabled()) {
				LOG.trace("", e);
			}
			return new byte[]{};
		}
	}
}
