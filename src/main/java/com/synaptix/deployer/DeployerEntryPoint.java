package com.synaptix.deployer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.CaseFormat;
import com.synaptix.deployer.exception.DeployerConfigurationException;
import com.synaptix.deployer.model.ConfigKey;
import com.synaptix.deployer.model.DeployerProperties;

import net.gnu.jargs.CmdLineParser;

public class DeployerEntryPoint {

	private static final Log LOG = LogFactory.getLog(DeployerEntryPoint.class);

	private static final String CONFIG_FILE_NAME = "deployer-config.ini"; //$NON-NLS-1$

	private static final String HTTP_PROXY_HOST = "http.proxyHost"; //$NON-NLS-1$
	private static final String HTTPS_PROXY_HOST = "https.proxyHost"; //$NON-NLS-1$
	private static final String HTTP_PROXY_PORT = "http.proxyPort"; //$NON-NLS-1$
	private static final String HTTPS_PROXY_PORT = "https.proxyPort"; //$NON-NLS-1$
	private static final Map<String, ProjectWorkspace> workspaceMap;

	private static ProxyAuth proxyAuth;

	private static volatile ProjectWorkspace currentWorkspace;

	static {
		workspaceMap = new HashMap<>();
	}

	private DeployerEntryPoint() {
	}

	public static synchronized void main(String[] args) throws Exception {
		LOG.info("Args: [--(g)itlabUrl=<url>] [--gitlab(T)oken=<token>] [--(u)surp=<hostname>] [--proxy(L)ogin=<login>] [--proxy(P)assword=<password>] [projectName]");
		LOG.info("Use *spy <projectName>* to spy a project");

		HierarchicalINIConfiguration configuration;
		File legacyConfigFile = new File("deployer-config.properties");
		boolean fromOldConfig = false;
		if (legacyConfigFile.exists()) {
			configuration = new HierarchicalINIConfiguration(legacyConfigFile);
			fromOldConfig = true;
			if (!legacyConfigFile.delete()) {
				LOG.warn("The legacy configuration file couldn't be deleted");
			}
		} else {
			configuration = loadConfiguration();
		}

		CmdLineParser cmdLineParser = new CmdLineParser();
		CmdLineParser.Option proxyLoginOption = cmdLineParser.addStringOption('L', "proxyLogin");
		CmdLineParser.Option proxyPasswordOption = cmdLineParser.addStringOption('P', "proxyPassword");
		CmdLineParser.Option gitlabUrlOption = cmdLineParser.addStringOption('g', "gitlabUrl");
		CmdLineParser.Option gitlabTokenOption = cmdLineParser.addStringOption('T', "gitlabToken");
		CmdLineParser.Option hostNameUsurpOption = cmdLineParser.addStringOption('u', "usurp");

		if (args != null) {
			cmdLineParser.parse(args);
		}

		String proxyLogin = (String) cmdLineParser.getOptionValue(proxyLoginOption, configuration.getProperty(ConfigKey.PROXY_LOGIN.toString()));
		String proxyPassword = (String) cmdLineParser.getOptionValue(proxyPasswordOption, configuration.getProperty(ConfigKey.PROXY_PASSWORD.toString()));
		initProxy(configuration, proxyLogin, proxyPassword);

		configuration.getSections().forEach(DeployerEntryPoint::loadProject);

		if (fromOldConfig) {
			spyFromOldConfiguration(configuration);
		}

		spyProject(args);

		LOG.info("Deployer initialized");

		parseUserInput();
	}

	/**
	 * @deprecated Will be removed in a future version: new configuration file: .ini
	 */
	@Deprecated
	private static void spyFromOldConfiguration(HierarchicalINIConfiguration configuration) throws ConfigurationException {
		String oldProject = configuration.getString("project"); // legacy
		if (oldProject != null) { // retro-compatibility, done only once
			DeployerProperties deployerProperties = new DeployerProperties();
			for (ConfigKey key : ConfigKey.values()) {
				if (!key.isGeneralKey()) {
					deployerProperties.setProperty(key, nvl(configuration.getString(key.toString()), key.getDefaultValue()));
					configuration.setProperty(key.toString(), null);
				}
			}
			configuration.setProperty("project", null);
			spyProject(oldProject, deployerProperties);

			saveConfiguration(configuration);

			registerParameters();
		}
	}

	private static void saveConfiguration(HierarchicalINIConfiguration configuration) {
		try (FileWriter writer = new FileWriter(CONFIG_FILE_NAME)) {
			configuration.save(writer);
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	private static void initProxy(HierarchicalINIConfiguration configuration, String proxyLogin, String proxyPassword) {
		setProperty(HTTP_PROXY_HOST, ConfigKey.HTTP_PROXY_HOST, configuration);
		setProperty(HTTPS_PROXY_HOST, ConfigKey.HTTPS_PROXY_HOST, configuration);
		setProperty(HTTP_PROXY_PORT, ConfigKey.HTTP_PROXY_PORT, configuration);
		setProperty(HTTPS_PROXY_PORT, ConfigKey.HTTPS_PROXY_PORT, configuration);

		// proxy http & proxy port are set with java system arguments (-D...)
		if (proxyLogin != null) {
			setProxyAuth(proxyLogin, proxyPassword != null ? proxyPassword.toCharArray() : null);
		}
	}

	private static void setProperty(String property, ConfigKey key, HierarchicalINIConfiguration configuration) {
		if (System.getProperty(property) == null) {
			String newValue = (String) configuration.getProperty(key.toString());
			if (newValue != null) {
				System.setProperty(property, newValue);
			}
		}
	}

	private static HierarchicalINIConfiguration loadConfiguration() {
		File propertyFile = new File(CONFIG_FILE_NAME);

		HierarchicalINIConfiguration configuration = null;
		try {
			configuration = new HierarchicalINIConfiguration(propertyFile);
		} catch (Exception e) {
			LOG.info("The property file could not be read!", e);
		}
		if (configuration == null) {
			configuration = new HierarchicalINIConfiguration();
		}
		return configuration;
	}

	private static <E> E nvl(E e1, E e2) {
		if (e1 != null) {
			return e1;
		}
		return e2;
	}

	private static synchronized void loadProject(String projectName) {
		if (projectName != null) {
			spyProject(projectName, null);
		}
	}

	private static synchronized void spyProject(String args) {
		spyProject(args.split(" "));
	}

	private static synchronized void spyProject(String[] args) {
		if (args != null && args.length != 0) {

			CmdLineParser cmdLineParser = new CmdLineParser();
			CmdLineParser.Option proxyLoginOption = cmdLineParser.addStringOption('L', "proxyLogin");
			CmdLineParser.Option proxyPasswordOption = cmdLineParser.addStringOption('P', "proxyPassword");
			CmdLineParser.Option gitlabUrlOption = cmdLineParser.addStringOption('g', "gitlabUrl");
			CmdLineParser.Option gitlabTokenOption = cmdLineParser.addStringOption('T', "gitlabToken");
			CmdLineParser.Option hostNameUsurpOption = cmdLineParser.addStringOption('u', "usurp");

			try {
				cmdLineParser.parse(args);
			} catch (Exception e) {
				LOG.error("Exception when parsing for arguments", e);
			}

			String projectName = args[args.length - 1];
			if (projectName.startsWith("-")) {
				projectName = null;
			}
			if (projectName != null) {
				String gitLabUrl = (String) cmdLineParser.getOptionValue(gitlabUrlOption, ConfigKey.GITLAB_URL.getDefaultValue());
				String gitLabToken = (String) cmdLineParser.getOptionValue(gitlabTokenOption, ConfigKey.GITLAB_TOKEN.getDefaultValue());
				String usurpHostName = (String) cmdLineParser.getOptionValue(hostNameUsurpOption);

				DeployerProperties deployerProperties = new DeployerProperties();
				deployerProperties.setProperty(ConfigKey.GITLAB_URL, gitLabUrl);
				deployerProperties.setProperty(ConfigKey.GITLAB_TOKEN, gitLabToken);
				deployerProperties.setProperty(ConfigKey.HOST_NAME, usurpHostName);

				spyProject(projectName, deployerProperties);
			}
		}
	}

	private static synchronized void spyProject(String projectName, DeployerProperties properties) {
		if (StringUtils.isNotBlank(projectName)) {
			ProjectWorkspace projectWorkspace = workspaceMap.get(projectName);
			if (projectWorkspace == null) {
				try {
					projectWorkspace = new ProjectWorkspace(projectName, properties != null ? properties : getProperties(projectName));
					String realName = projectWorkspace.getProjectName();
					workspaceMap.put(realName, projectWorkspace);
				} catch (DeployerConfigurationException e) {
					LOG.error("The project couldn't be found. Use command *spy <projectName>* to change change or reload the project");
					if (LOG.isDebugEnabled()) {
						LOG.debug("spyProject Exception", e);
					}
					projectWorkspace = currentWorkspace;
					workspaceMap.remove(projectName);
				}
			}

			currentWorkspace = projectWorkspace;
		} else {
			LOG.info("Active workspaces: " + workspaceMap.keySet());
			LOG.info("Current is: " + (currentWorkspace != null ? currentWorkspace.getProjectName() : "<none>"));
		}
	}

	private static DeployerProperties getProperties(String workspace) {
		HierarchicalINIConfiguration configuration = loadConfiguration();

		SubnodeConfiguration section = configuration.getSection(workspace);

		DeployerProperties deployerProperties = new DeployerProperties();
		for (ConfigKey key : ConfigKey.values()) {
			if (!key.isGeneralKey()) {
				deployerProperties.setProperty(key, nvl(section.getString(key.toString()), key.getDefaultValue()));
			}
		}
		return deployerProperties;
	}

	private static void setProxyUrl(String proxyUrl) {
		System.setProperty(HTTP_PROXY_HOST, proxyUrl);
		System.setProperty(HTTPS_PROXY_HOST, proxyUrl);
	}

	private static void setProxyPort(Integer proxyPort) {
		System.setProperty(HTTP_PROXY_PORT, Integer.toString(proxyPort));
		System.setProperty(HTTPS_PROXY_PORT, Integer.toString(proxyPort));
	}

	private static void proxyAuth(String login) {
		char[] passwords = null;
		if (System.console() != null) {
			passwords = System.console().readPassword("[%s]", "Password");
		} else {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			try {
				LOG.info("Enter your password");
				passwords = reader.readLine().toCharArray();
			} catch (IOException e) {
				LOG.error("", e);
			}
		}
		setProxyAuth(login, passwords);
	}

	private static synchronized void setProxyAuth(String login, char[] password) {
		proxyAuth = new ProxyAuth(login, password);
		Authenticator.setDefault(proxyAuth);
	}

	private static void parseUserInput() {
		try (Scanner scan = new Scanner(System.in)) {
			boolean exit = false;
			do {
				String line = getNextLine(scan);
				if (line == null) {
					continue;
				}
				String[] words = line.split(" ");
				List<String> l = new ArrayList<>(Arrays.asList(words));
				String w1 = l.get(0);

				Commands command = Commands.find(w1);
				if (command == Commands.EXIT) {
					exit = true;
				} else if (command != null) {
					l.remove(0);
					command.execute(String.join(" ", l));
				} else {
					Commands.displayCommands();
				}
			} while (!exit);
		}
	}

	private static String getNextLine(Scanner scan) {
		try {
			return scan.nextLine();
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("", e);
			}
			return null;
		}
	}

	private static void registerParameters() {
		File propertyFile = new File(CONFIG_FILE_NAME);

		HierarchicalINIConfiguration configuration;
		try {
			configuration = new HierarchicalINIConfiguration(propertyFile);
		} catch (ConfigurationException e) {
			LOG.error("Couldn't get the properties file.", e);
			return;
		}

		configuration.setProperty(ConfigKey.HTTP_PROXY_HOST.toString(), System.getProperty(HTTP_PROXY_HOST));
		configuration.setProperty(ConfigKey.HTTPS_PROXY_HOST.toString(), System.getProperty(HTTPS_PROXY_HOST));
		configuration.setProperty(ConfigKey.HTTP_PROXY_PORT.toString(), System.getProperty(HTTP_PROXY_PORT));
		configuration.setProperty(ConfigKey.HTTPS_PROXY_PORT.toString(), System.getProperty(HTTPS_PROXY_PORT));
		if (proxyAuth != null && proxyAuth.getPasswordAuthentication() != null) {
			configuration.setProperty(ConfigKey.PROXY_LOGIN.toString(), proxyAuth.getPasswordAuthentication().getUserName());
			configuration.setProperty(ConfigKey.PROXY_PASSWORD.toString(), String.valueOf(proxyAuth.getPasswordAuthentication().getPassword()));
		}

		if (currentWorkspace != null) {
			DeployerProperties properties = currentWorkspace.getParameters();

			SubnodeConfiguration section = configuration.getSection(currentWorkspace.getProjectName());

			for (ConfigKey key : ConfigKey.values()) {
				if (!key.isGeneralKey()) {
					section.setProperty(key.toString(), properties.getProperty(key));
				}
			}
		}
		try (FileWriter writer = new FileWriter(propertyFile)) {
			configuration.save(writer);
			propertyFile.setReadable(true, true);
			propertyFile.setWritable(true, true);
			propertyFile.setExecutable(false, false);
		} catch (Exception e) {
			LOG.error("", e);
		}
	}

	private static void destroy(String args) {
		boolean current = StringUtils.isBlank(args);
		String projectName = !current ? args : currentWorkspace.getProjectName();
		if (projectName != null) {
			ProjectWorkspace projectWorkspace = workspaceMap.remove(projectName);
			if (projectWorkspace != null) {
				projectWorkspace.destroy();
				LOG.info(String.format("Workspace %s has been destroyed!", projectName));
			}
			if (current) {
				currentWorkspace = null;
			}
			HierarchicalINIConfiguration configuration = loadConfiguration();
			if (configuration.getSection(projectName) != null) {
				configuration.getSection(projectName).clear();
				configuration.setProperty(projectName + ".", null);

				saveConfiguration(configuration);
			}
		}
	}

	private enum CommandFamily {

		/***/
		GENERAL,
		/***/
		PROJECT,
		/***/
		WORKSPACE,
		/***/
		CONFIGURATION

	}

	private enum Commands {

		/***/
		HELP(CommandFamily.GENERAL, "*help <command>* - Display help on command", args -> {
			Commands c2 = find(args);
			if (c2 != null) {
				LOG.info(String.format("%s (%s): %s", c2.name(), c2.commandFamily.toString().toLowerCase(), c2.description));
			} else {
				Commands h = find("help");
				LOG.info(String.format("%s: %s", h.name(), h.description));
				displayCommands();
			}
		}),
		/***/
		EXIT(CommandFamily.GENERAL, "Exit the deployer", null),
		/***/
		SPY(CommandFamily.WORKSPACE, "*spy <projectName>* - Initialize the checker on given project. If <projectName> is null, lists the projects. Arguments [--(g)itlabUrl=<url>] [--gitlab(T)oken=<token>] can be provided.", DeployerEntryPoint::spyProject),
		/***/
		DESTROY(CommandFamily.WORKSPACE, "*destroy <projectName> - Destroy the given workspace, or current workspace if <projectName> is null", DeployerEntryPoint::destroy),
		/***/
		AUTO(CommandFamily.PROJECT, "*auto off/on* - Start or stop the checher thread for automatic deliveries", args -> {
			Boolean running = BooleanUtils.toBooleanObject(args);
			if (running == null) {
				LOG.error("Invalid parameter: " + args);
			} else {
				currentWorkspace.setRunning(running);
			}
		}),
		/***/
		START(CommandFamily.PROJECT, "Start the application", args -> currentWorkspace.start(args)),
		/***/
		STOP(CommandFamily.PROJECT, "Stop the application", args -> currentWorkspace.stop(args)),
		/***/
		DEPLOY(CommandFamily.PROJECT, "*deploy <refName>* - Deploy a ref name", args -> currentWorkspace.deploy(args)),
		/***/
		TEST(CommandFamily.PROJECT, "*test <refName>* - Run a full smoke tests campaign", args -> currentWorkspace.test(args)),
		/***/
		CHECKER_STATUS(CommandFamily.PROJECT, "Display the checker status", args -> currentWorkspace.displayStatus()),
		/***/
		KILL(CommandFamily.PROJECT, "*kill <id>* Interrupt a deploy thread. Use *checkerStatus* to get the list", args -> currentWorkspace.kill(args)),
		/***/
		USURP(CommandFamily.PROJECT, "*usurp <hostname>* - Usurp a hostname", args -> currentWorkspace.setHostName(args)),
		/***/
		PROXY_URL(CommandFamily.CONFIGURATION, "*proxyUrl <proxyUrl>* - Sets the proxy url", DeployerEntryPoint::setProxyUrl),
		/***/
		PROXY_PORT(CommandFamily.CONFIGURATION, "*proxyPort <proxyPort>* - Sets the proxy port", args -> setProxyPort(args != null && args.matches("^[0-9]+$") ? Integer.parseInt(args) : null)),
		/***/
		PROXY_AUTH(CommandFamily.CONFIGURATION, "*proxyAuth <proxyLogin>* - Sets the proxy login", DeployerEntryPoint::proxyAuth, true),
		/***/
		REGISTER(CommandFamily.CONFIGURATION, "Register configuration in a properties file, which can be used by the next deployer", args -> registerParameters());

		private final CommandFamily commandFamily;
		private final String description;
		private final ICommand command;
		private final boolean infoWhenDone;

		Commands(CommandFamily commandFamily, String description, ICommand command) {
			this(commandFamily, description, command, false);
		}

		Commands(CommandFamily commandFamily, String description, ICommand command, boolean infoWhenDone) {
			this.commandFamily = commandFamily;
			this.description = description;
			this.command = command;
			this.infoWhenDone = infoWhenDone;
		}

		static Commands find(String command) {
			for (Commands value : values()) {
				if (value.matches(command)) {
					return value;
				}
			}
			return null;
		}

		public static void displayCommands() {
			LOG.info("Supported commands: " + Arrays.toString(Commands.values()));
		}

		@Override
		public String toString() {
			return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, this.name());
		}

		boolean matches(String command) {
			return this.toString().equalsIgnoreCase(command);
		}

		public void execute(String args) {
			if (commandFamily == CommandFamily.PROJECT && currentWorkspace == null) {
				LOG.error("Cannot do that, you must first spy a project!");
				return;
			}
			try {
				LOG.info(String.format("> %s %s", this.toString(), args));
				if (command != null) {
					command.execute(args);
				}
				if (infoWhenDone) {
					LOG.info(String.format("> %s called", this.toString()));
				}
			} catch (Exception e) {
				LOG.error("", e);
			}
		}
	}

	private interface ICommand {

		void execute(String args);

	}

	private static class ProxyAuth extends Authenticator {

		private PasswordAuthentication auth;

		private ProxyAuth(String user, char[] password) {
			auth = new PasswordAuthentication(user, password == null ? new char[]{} : password);
		}

		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return auth;
		}
	}
}
