package com.synaptix.deployer;

import com.synaptix.gitlab.api.models.builds.GitLabBuild;
import com.synaptix.gitlab.api.models.projects.GitLabProject;

public interface IDeployer {

	enum Mode {

		AUTOMATIC, FORCE, CHECK;

	}

	/**
	 * Fired when a new build is found, whether it has artifacts or not
	 */
	void fireNewBuild(GitLabProject project, GitLabBuild build, Mode mode);

	/**
	 * Start the application with the start commands from definition of given ref
	 */
	void start(GitLabProject project, String ref);

	/**
	 * Stop the application with the stop commands from definition of given ref
	 */
	void stop(GitLabProject project, String ref);

	/**
	 * Test the application with the smoke test entrypoints from definition of given ref
	 */
	void test(String ref);

	/**
	 * Set the hostname of the deployer, to usurp another
	 */
	void setHostName(String hostName);

}
