package com.synaptix.deployer.model;

import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.synaptix.component.IComponent;

/**
 * Created by NicolasP on 14/04/2016.
 */
public interface IEnvironment extends IComponent {

	@JsonIgnore
	// not sure this annotation works in components
	String getName();

	@JsonIgnore
	void setName(String name);

	String getHost();

	void setHost(String host);

	String getLogfile();

	void setLogfile(String logfile);

	/**
	 * Key is the instance, value is the instance definition
	 */
	Map<String, IInstance> getInstance();

	void setInstance(Map<String, IInstance> instance);

	List<String> getSmokeEntrypoint();

	void setSmokeEntrypoint(List<String> smokeEntrypoint);

}
