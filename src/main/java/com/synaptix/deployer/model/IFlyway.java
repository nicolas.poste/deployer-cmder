package com.synaptix.deployer.model;

import com.synaptix.component.IComponent;

/**
 * Created by NicolasP on 13/09/2016.
 */
public interface IFlyway extends IComponent {

	String getConfigFile();

	void setConfigFile(String configFile);

}
