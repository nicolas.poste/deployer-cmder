package com.synaptix.deployer.model;

import java.util.List;

import com.synaptix.component.IComponent;

/**
 * Created by NicolasP on 14/04/2016.
 */
public interface IInstance extends IComponent {

	String getFolder();

	void setFolder(String folder);

	List<String> getArtifact();

	void setArtifact(List<String> artifact);

}
