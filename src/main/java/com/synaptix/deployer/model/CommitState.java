package com.synaptix.deployer.model;

/**
 * Created by NicolasP on 23/05/2016.
 */
public enum CommitState {

	PENDING, RUNNING, SUCCESS, FAILED, CANCELED;

	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
}
