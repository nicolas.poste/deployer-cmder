package com.synaptix.deployer.model;

import java.util.Properties;

/**
 * Created by NicolasP on 27/06/2016.
 */
public class DeployerProperties extends Properties {

	public String getProperty(ConfigKey configKey) {
		return super.getProperty(configKey.toString());
	}

	public Object setProperty(ConfigKey configKey, String value) {
		if (value == null) {
			return super.remove(configKey.toString());
		}
		return super.setProperty(configKey.toString(), value);
	}
}
