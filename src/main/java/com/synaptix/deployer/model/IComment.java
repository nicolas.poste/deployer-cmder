package com.synaptix.deployer.model;

import com.synaptix.component.IComponent;

/**
 * Created by NicolasP on 14/04/2016.
 */
public interface IComment extends IComponent {

	String getDescription();

	void setDescription(String description);

}
