package com.synaptix.deployer.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;

import com.synaptix.gitlab.api.models.builds.GitLabBuild;

/**
 * Created by NicolasP on 14/04/2016.
 */
public class Deploy {

	private Map<String, String> artifacts;

	private Map<String, IEnvironment> environment;

	private List<IJob> jobs;

	private GitLabBuild build;

	@JsonAnySetter
	void setJobs(String name, IJob job) {
		if (name.startsWith(".")) {
			return;
		}
		if (jobs == null) {
			jobs = new ArrayList<>();
		}
		job.setName(name);
		jobs.add(job);
	}

	public List<IJob> getJobs() {
		return jobs;
	}

	public Map<String, String> getArtifacts() {
		return artifacts;
	}

	public void setArtifacts(Map<String, String> artifacts) {
		this.artifacts = artifacts;
	}

	public Map<String, IEnvironment> getEnvironment() {
		return environment;
	}

	public void setEnvironment(Map<String, IEnvironment> environment) {
		this.environment = environment;
	}

	@JsonIgnore
	public GitLabBuild getBuild() {
		return build;
	}

	@JsonIgnore
	public void setBuild(GitLabBuild build) {
		this.build = build;
	}
}
