package com.synaptix.deployer.model;

import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.synaptix.component.IComponent;

/**
 * Created by NicolasP on 14/04/2016.
 */
public interface IJob extends IComponent {

	@JsonIgnore
	String getName();

	@JsonIgnore
	void setName(String name);

	@JsonIgnore
	String getCurrentStep();

	@JsonIgnore
	void setCurrentStep(String currentStep);

	boolean isAutomatic();

	void setAutomatic(boolean automatic);

	List<String> getEnvironment();

	void setEnvironment(List<String> environment);

	List<String> getOnly();

	void setOnly(List<String> only);

	IComment getComment();

	void setComment(IComment mattermost);

	/**
	 * Key is the instance, value is the list of commands
	 */
	Map<String, List<String>> getStop();

	void setStop(Map<String, List<String>> stop);

	IFlyway getFlyway();

	void setFlyway(IFlyway flyway);

	/**
	 * Key is the instance, value is the list of commands
	 */
	Map<String, List<String>> getClean();

	void setClean(Map<String, List<String>> clean);

	/**
	 * Key is the instance, value is the list of commands
	 */
	Map<String, List<String>> getStart();

	void setStart(Map<String, List<String>> start);

}
