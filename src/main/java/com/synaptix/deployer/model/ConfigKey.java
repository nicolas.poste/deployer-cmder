package com.synaptix.deployer.model;

import com.google.common.base.CaseFormat;

/**
 * Created by NicolasP on 27/06/2016.
 */
public enum ConfigKey {

	/***/
	HTTP_PROXY_HOST(true), HTTPS_PROXY_HOST(true), HTTP_PROXY_PORT(true), HTTPS_PROXY_PORT(true), PROXY_LOGIN(true), PROXY_PASSWORD(true),
	/***/
	GITLAB_URL("https://gitlab.talanlabs.com/"), GITLAB_TOKEN("yomuShFU_KgPPfZ7YkgJ"),
	/***/
	HOST_NAME;

	private final boolean isGeneralKey;

	private final String defaultValue;

	ConfigKey() {
		this.isGeneralKey = false;
		this.defaultValue = null;
	}

	ConfigKey(boolean isGeneralKey) {
		this.isGeneralKey = isGeneralKey;
		this.defaultValue = null;
	}

	ConfigKey(String defaultValue) {
		this.isGeneralKey = false;
		this.defaultValue = defaultValue;
	}

	public boolean isGeneralKey() {
		return isGeneralKey;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	@Override
	public String toString() {
		return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, super.toString());
	}
}
