package com.synaptix.deployer.test.helper;

import java.io.InputStream;
import java.util.Map;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.yaml.snakeyaml.Yaml;

import com.synaptix.componentresteasy.SynaptixComponentModule;
import com.synaptix.deployer.model.Deploy;

/**
 * Created by NicolasP on 11/08/2016.
 */
public class YmlParser {

	public static Deploy parseYml(InputStream resourceAsStream) {
		Yaml yaml = new Yaml();
		Map<String, Object> map = (Map<String, Object>) yaml.load(resourceAsStream);

		final ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new SynaptixComponentModule());
		mapper.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);

		return mapper.convertValue(map, Deploy.class);
	}
}
