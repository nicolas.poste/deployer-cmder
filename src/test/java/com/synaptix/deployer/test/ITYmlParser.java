package com.synaptix.deployer.test;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.synaptix.deployer.model.Deploy;
import com.synaptix.deployer.model.IEnvironment;
import com.synaptix.deployer.model.IJob;

import com.synaptix.deployer.test.helper.YmlParser;

/**
 * Created by NicolasP on 14/04/2016.
 */
public class ITYmlParser {

	@Test
	public void testYmlParser() {
		String ymlFileName = ".deploy-war.yml";
		final Deploy deploy = YmlParser.parseYml(getClass().getResourceAsStream(ymlFileName));

		deploy.getEnvironment();
		deploy.getJobs();

		Assert.assertNotNull(deploy.getJobs());
		Assert.assertTrue(deploy.getJobs().size() > 0);
		IJob job = deploy.getJobs().get(0);
		Assert.assertEquals("deploy-dev-job", job.getName());

		Assert.assertTrue(job.isAutomatic());
		Assert.assertNotNull(job.getEnvironment());
		Assert.assertTrue(job.getEnvironment().size() > 0);
		Assert.assertTrue(job.getEnvironment().contains("dev"));
		Assert.assertNotNull(job.getComment());
		Assert.assertTrue(job.getStart().size() > 0);

		Map<String, IEnvironment> environments = deploy.getEnvironment();

		Assert.assertTrue(environments.containsKey("dev"));

		IEnvironment environment = environments.get("dev");

		Assert.assertEquals("my-dev.host", environment.getHost());
		Assert.assertNotNull(environment.getSmokeEntrypoint());
		Assert.assertTrue(environment.getSmokeEntrypoint().size() >= 1);

		Assert.assertNotNull(deploy.getArtifacts());
		Assert.assertTrue(deploy.getArtifacts().containsKey("client"));
		Assert.assertEquals("client/target/my-app-client.war", deploy.getArtifacts().get("client"));
	}
}
