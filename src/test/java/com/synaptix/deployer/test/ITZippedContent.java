package com.synaptix.deployer.test;

import java.io.InputStream;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.synaptix.deployer.Deployer;

/**
 * Created by NicolasP on 10/08/2016.
 */
public class ITZippedContent {

	@Test
	public void testZippedContent() throws Exception {
		Deployer deployer = new Deployer(null, null);

		InputStream resourceAsStream = getClass().getResourceAsStream("scripts.zip");
		byte[] bytes = IOUtils.toByteArray(resourceAsStream);
		Map<String, byte[]> fileMap = deployer.getZippedContent(bytes, "scripts/*.sql");

		Assert.assertEquals(3, fileMap.size());
		Assert.assertNotNull(fileMap.get("scripts/dev/script1.sql"));
		Assert.assertEquals("\r\nselect sysdate from dual;\r\n", new String(fileMap.get("scripts/dev/script1.sql")));
	}
}
