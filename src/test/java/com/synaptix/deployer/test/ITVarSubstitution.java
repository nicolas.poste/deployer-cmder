package com.synaptix.deployer.test;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.synaptix.deployer.Deployer;
import com.synaptix.deployer.model.Deploy;

import com.synaptix.deployer.test.helper.YmlParser;

/**
 * Created by NicolasP on 10/08/2016.
 */
public class ITVarSubstitution {

	@Test
	public void testVarSubstitution() throws Exception {
		Deployer deployer = new Deployer(null, null);

		Map<String, String> varMap = new HashMap<>();
		varMap.put("ENVIRONMENT", "dev");

		String s = deployer.evaluateString("$environment/ok", varMap);

		Assert.assertEquals("dev/ok", s);

		String ymlFileName = ".deploy.yml";
		final Deploy deploy = YmlParser.parseYml(getClass().getResourceAsStream(ymlFileName));

		InputStream resourceAsStream = getClass().getResourceAsStream("scripts.zip");
		byte[] bytes = IOUtils.toByteArray(resourceAsStream);

		Assert.assertEquals(2, deploy.getArtifacts().size());
		for (String artifact : deploy.getArtifacts().values()) {
			Map<String, byte[]> zippedContent = deployer.getZippedContent(bytes, deployer.evaluateString(artifact, varMap));
			Assert.assertEquals(1, zippedContent.size());
		}
	}
}
