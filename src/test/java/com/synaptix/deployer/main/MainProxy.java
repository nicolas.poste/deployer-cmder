package com.synaptix.deployer.main;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

/**
 * Created by NicolasP on 20/05/2016.
 */
public class MainProxy {

	public static void main(String[] args) {
		System.setProperty("http.proxyHost", "vs-proxies.inetgefco.net");
		System.setProperty("https.proxyHost", "vs-proxies.inetgefco.net");
		System.setProperty("http.proxyPort", "8080");
		System.setProperty("https.proxyPort", "8080");

		Authenticator.setDefault(new ProxyAuth("gefco-acct\\e413915", "PoNicolas".toCharArray()));

		CloseableHttpClient client = HttpClients.createSystem();

		try {
			HttpPost post = new HttpPost("https://mattermost.talanlabs.com/hooks/o1xyqbid8tywdga5ayeps3q9rr");
			JSONObject json = new JSONObject();
			json.put("text", "test");
			json.put("username", "Deployeur");
			json.put("icon_url", "https://gitlab.talanlabs.com/uploads/project/avatar/120/deployer.png");
			post.addHeader("content-type", "application/json");
			post.setEntity(new StringEntity(json.toString()));
			HttpResponse httpResponse = client.execute(post);
			System.out.println("Mattermost answered: " + IOUtils.toString(httpResponse.getEntity().getContent(), "UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static class ProxyAuth extends Authenticator {

		private PasswordAuthentication auth;

		private ProxyAuth(String user, char[] password) {
			auth = new PasswordAuthentication(user, password == null ? new char[]{} : password);
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return auth;
		}
	}
}
