package com.synaptix.deployer.main;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.CaseFormat;
import com.synaptix.deployer.DeployerEntryPoint;
import com.synaptix.gitlab.api.GitLabAPI;

/**
 * Created by NicolasP on 14/06/2016.
 */
public class MainCallApi {

	private static final Log LOG = LogFactory.getLog(DeployerEntryPoint.class);

	private static final String CONFIG_FILE_NAME = "deployer-config.properties"; //$NON-NLS-1$

	private static final String HTTP_PROXY_HOST = "http.proxyHost"; //$NON-NLS-1$
	private static final String HTTPS_PROXY_HOST = "https.proxyHost"; //$NON-NLS-1$
	private static final String HTTP_PROXY_PORT = "http.proxyPort"; //$NON-NLS-1$
	private static final String HTTPS_PROXY_PORT = "https.proxyPort"; //$NON-NLS-1$

	public static void main(String[] args) {
		DeployerProperties properties = loadProperties();

		String gitLabUrl = nvl(properties.getProperty(ConfigKey.GITLAB_URL), "https://gitlab.com/");
		String gitLabToken = nvl(properties.getProperty(ConfigKey.GITLAB_TOKEN), "yomuShFU_KgPPfZ7YkgJ");
		String usurpHostName = properties.getProperty(ConfigKey.HOST_NAME);
		String proxyLogin = properties.getProperty(ConfigKey.PROXY_LOGIN);
		String proxyPassword = properties.getProperty(ConfigKey.PROXY_PASSWORD);

		initProxy(properties, proxyLogin, proxyPassword);

		GitLabAPI api = GitLabAPI.connect(gitLabUrl, gitLabToken);
		try {
			api.getGitLabAPICommits().postCommitStatus("namespace/projectname", "66ea45e6d94f2abd37ae570b3957c3eec4219e21", "success", "master", "smoke-test_integ", null, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void initProxy(DeployerProperties properties, String proxyLogin, String proxyPassword) {
		if (System.getProperty(HTTP_PROXY_HOST) == null) {
			String property = properties.getProperty(ConfigKey.HTTP_PROXY_HOST);
			if (property != null) {
				System.setProperty(HTTP_PROXY_HOST, property);
			}
		}
		if (System.getProperty(HTTPS_PROXY_HOST) == null) {
			String property = properties.getProperty(ConfigKey.HTTPS_PROXY_HOST);
			if (property != null) {
				System.setProperty(HTTPS_PROXY_HOST, property);
			}
		}
		if (System.getProperty(HTTP_PROXY_PORT) == null) {
			String property = properties.getProperty(ConfigKey.HTTP_PROXY_PORT);
			if (property != null) {
				System.setProperty(HTTP_PROXY_PORT, property);
			}
		}
		if (System.getProperty(HTTPS_PROXY_PORT) == null) {
			String property = properties.getProperty(ConfigKey.HTTPS_PROXY_PORT);
			if (property != null) {
				System.setProperty(HTTPS_PROXY_PORT, property);
			}
		}
		// proxy http & proxy port are set with java system arguments (-D...)
		if (proxyLogin != null) {
			setProxyAuth(proxyLogin, proxyPassword != null ? proxyPassword.toCharArray() : null);
		}
	}

	private static synchronized void setProxyAuth(String login, char[] password) {
		Authenticator.setDefault(new ProxyAuth(login, password));
	}

	private static <E> E nvl(E e1, E e2) {
		if (e1 != null) {
			return e1;
		}
		return e2;
	}

	private static DeployerProperties loadProperties() {
		File propertyFile = new File(CONFIG_FILE_NAME);
		DeployerProperties properties = new DeployerProperties();
		if (propertyFile.exists()) {
			try (FileReader reader = new FileReader(propertyFile)) {
				properties.load(reader);
			} catch (Exception e) {
				LOG.info("The property file could not be read!", e);
			}
		}
		return properties;
	}

	private enum ConfigKey {
		HTTP_PROXY_HOST, HTTPS_PROXY_HOST, HTTP_PROXY_PORT, HTTPS_PROXY_PORT, GITLAB_URL, GITLAB_TOKEN, PROJECT, PROXY_LOGIN, PROXY_PASSWORD, HOST_NAME;

		@Override
		public String toString() {
			return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, super.toString());
		}
	}

	private static class ProxyAuth extends Authenticator {

		private PasswordAuthentication auth;

		private ProxyAuth(String user, char[] password) {
			auth = new PasswordAuthentication(user, password == null ? new char[]{} : password);
		}

		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return auth;
		}
	}

	private static class DeployerProperties extends Properties {

		public String getProperty(ConfigKey configKey) {
			return super.getProperty(configKey.toString());
		}

		public Object setProperty(ConfigKey configKey, String value) {
			if (value == null) {
				return super.remove(configKey.toString());
			}
			return super.setProperty(configKey.toString(), value);
		}
	}
}
