package com.synaptix.deployer.main;

import java.io.FileReader;
import java.util.Properties;

import org.flywaydb.core.Flyway;

/**
 * Created by NicolasP on 09/08/2016.
 */
public class FlywayMigrate {

	public static void main(String[] args) throws Exception {
		Flyway flyway = new Flyway();

		Properties properties = new Properties();
		try (FileReader reader = new FileReader("src/test/resources/conf/flyway.properties")) {
			properties.load(reader);
		} catch (Exception e) {
			throw e;
		}

		flyway.configure(properties);

//		flyway.setDataSource("jdbc:postgresql://localhost:5432/lisea", "lisea", "lisea");
//		flyway.setLocations("filesystem:D:\\flyway");
//		flyway.setSchemas("lisea");

		flyway.setBaselineOnMigrate(true);
		flyway.setOutOfOrder(true);
		int nb = flyway.migrate();

		System.out.println("nb: " + nb);
	}
}
